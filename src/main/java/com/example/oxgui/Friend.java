/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.example.oxgui;

import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author aof2a
 */
public class Friend {
    private int id;
    private String name;
    private int age;
    private String tel;
    private static int lastid = 0;
    
    public Friend(String name, int age, String tel){
        this.id = lastid++;
        this.name = name;
        this.age  = age;
        this.tel = tel;
    }
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) throws Exception {
        if(age<0){
            throw new Exception();
        }
        this.age = age;
        
    }

    public String getTel() {
        return tel;
    }

    public void setTel(String tel) {
        this.tel = tel;
    }
    public static void main(String[] args)  {
        Friend f = new Friend("Pom",70,"1234567");
        
        try {
            f.setAge(-1);
        } catch (Exception ex) {
            System.out.println("age is lower than 0");
        }
        System.out.println(""+f);
    }
    
    
}
